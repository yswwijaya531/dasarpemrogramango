package common

type Users struct {
	ID           int    `json:"id,omitempty"`
	NamaDepan    string `json:"nama_depan,omitempty"`
	NamaBelakang string `json:"nama_belakang,omitempty"`
	Email        string `json:"email, omitempty"`
	Username     string `json:"username, omitempty"`
	Password     string `json:"password, omitempty"`
}

type Customers struct {
	CustomerID   string `json:"customerID, omitempty"`
	SupplierID   string `json:"supplierID, omitempty"`
	CompanyName  string `json:"companyName, omitempty"`
	ContactName  string `json:"contactName, omitempty"`
	ContactTitle string `json:"contactTitle, omitempty"`
	Address      string `json:"address, omitempty"`
	City         string `json:"city, omitempty"`
	Country      string `json:"country, omitempty"`
	Phone        string `json:"phone, omitempty"`
	PostalCode   string `json:"postalCode, omitempty"`
}

type Suppliers struct {
	SupplierID   string `json:"supplierID, omitempty"`
	CompanyName  string `json:"companyName, omitempty"`
	ContactName  string `json:"contactName, omitempty"`
	ContactTitle string `json:"contactTitle, omitempty"`
	Address      string `json:"address, omitempty"`
	City         string `json:"city, omitempty"`
	Country      string `json:"country, omitempty"`
	Phone        string `json:"phone, omitempty"`
	PostalCode   string `json:"postalCode, omitempty"`
}

type Employees struct {
	LastName  string `json:"lastName"`
	FirstName string `json:"firstName"`
	Title     string `json:"title"`
	Address   string `json:"address"`
}
