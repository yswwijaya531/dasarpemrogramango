package routes

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"
)

//Init ...
func Init() *echo.Echo {

	e := echo.New()
	e.Use(middleware.Logger())

	//UserRoute ...
	UserRoute(e.Group("/user"))

	//CustomersRoute ...
	CustomersRoute(e.Group("/customers"))

	//EmployeesRoute ...
	EmployeesRoute(e.Group("/employees"))

	//EmployeesRoute ...
	SuppliersRoute(e.Group("/suppliers"))

	e.GET("/swagger/*", echoSwagger.WrapHandler)

	return e
}
