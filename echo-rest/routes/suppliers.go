package routes

import (
	"echo-rest/controllers"

	"github.com/labstack/echo/v4"
)

//CustomersRoute ...
func SuppliersRoute(g *echo.Group) {

	g.GET("/lists", controllers.FetchSuppliers)

	g.POST("/list", controllers.FetchCustomer)

	// g.POST("/add", controllers.StoreCustomer)

	// g.POST("/update", controllers.UpdateCustomer)

	// g.POST("/delete", controllers.DeleteCustomer)

}
