package routes

import (
	"echo-rest-sqlx/controllers"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

//Init ...
func Init() *echo.Echo {

	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.POST("/travelList", controllers.FetchTravels)

	e.POST("/travelUser", controllers.FetchUser)

	return e
}
